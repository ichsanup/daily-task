<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Sign-in With Google</name>
   <tag></tag>
   <elementGuidId>d3e16d15-df14-4cc4-95a0-f26c85e029a8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='buttonGoogleTrack']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#buttonGoogleTrack</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>f307537d-f138-4206-b304-d39ee26db936</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>buttonGoogleTrack</value>
      <webElementGuid>80aff80a-20c8-40ea-b118-32869f6bdb20</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>2fabf913-d14e-4690-92e2-6926f6f533e2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn wm-all-events btn-block bg-white</value>
      <webElementGuid>0e15d9df-d6fa-4b0d-8e86-9907cadc63ae</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                            
                                            Sign-in With Google
                                        </value>
      <webElementGuid>81e6f050-0972-4836-80bc-03f71191247d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;buttonGoogleTrack&quot;)</value>
      <webElementGuid>a407299a-d7eb-45d0-b8d1-cdc5155053af</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='buttonGoogleTrack']</value>
      <webElementGuid>5e878249-0560-4d1e-a285-3688e2e52f62</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Daftar'])[1]/following::button[1]</value>
      <webElementGuid>d6886c56-6f33-46bb-be56-c1585ee465be</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='syarat dan ketentuan'])[1]/following::button[2]</value>
      <webElementGuid>77a640e3-288c-4580-a813-f0ad35e9c4fe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Sign-in With Google']/parent::*</value>
      <webElementGuid>aaae66ba-cb6e-48a2-8601-ca592297ccbb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a/button</value>
      <webElementGuid>e7cab501-561c-4a98-b73d-b6a6356a1f46</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = 'buttonGoogleTrack' and @type = 'button' and (text() = '
                                            
                                            Sign-in With Google
                                        ' or . = '
                                            
                                            Sign-in With Google
                                        ')]</value>
      <webElementGuid>1ca02147-9cac-4850-b71e-baef3d1abc0c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
