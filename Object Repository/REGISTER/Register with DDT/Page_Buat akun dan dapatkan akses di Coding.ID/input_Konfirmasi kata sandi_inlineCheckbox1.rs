<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Konfirmasi kata sandi_inlineCheckbox1</name>
   <tag></tag>
   <elementGuidId>399bfae0-fc03-41d6-89b9-700f5e7d6a9d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='inlineCheckbox1']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#inlineCheckbox1</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>44913dda-a512-4aad-b470-3acb78042aa7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-check-input</value>
      <webElementGuid>03187034-b2ef-4c98-8e42-e816ec8b421a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>b3c57958-9c58-4935-82c9-c89635b84e6d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>inlineCheckbox1</value>
      <webElementGuid>c63f8acd-350a-4b5f-be3f-dbc6d1f9fa9b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>option1</value>
      <webElementGuid>2180ff6e-7b96-40e8-b709-0d896424424d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;inlineCheckbox1&quot;)</value>
      <webElementGuid>77f64d52-8d39-4e93-bcb5-0ce9a2ab6bbb</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='inlineCheckbox1']</value>
      <webElementGuid>9f59ebc9-6229-4b9a-bb5a-e5ec85ae2869</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[7]/div/input</value>
      <webElementGuid>2040ce71-81c3-4621-9e0a-2c40738060c0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'checkbox' and @id = 'inlineCheckbox1']</value>
      <webElementGuid>cc6aa2ca-a09c-47ea-ac32-f1ed5fe7ed83</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
