<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Daftar</name>
   <tag></tag>
   <elementGuidId>3d9a4ac5-8e01-40a3-afd2-b2832ed317bd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='buttonRegisterTrack']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#buttonRegisterTrack</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>cf095ae6-5b6a-4a56-84ce-92addde0683f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>buttonRegisterTrack</value>
      <webElementGuid>37211a35-bd9f-481a-aa8e-3c282255f843</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>03ab92ec-8a10-4ebe-9177-7e9da5dc2f9c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-block btn-primary</value>
      <webElementGuid>0825db24-c58d-47a5-8acb-ce9910eb23bf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                            Daftar
                                        </value>
      <webElementGuid>b0711e01-a2c5-4fde-a80a-d611c8b2727e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;buttonRegisterTrack&quot;)</value>
      <webElementGuid>af609005-5ce0-4705-a455-3bf6140a2c92</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='buttonRegisterTrack']</value>
      <webElementGuid>3ccbec39-3140-4cb1-acf7-dda2ba738069</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='syarat dan ketentuan'])[1]/following::button[1]</value>
      <webElementGuid>62e25b32-bb51-459a-b693-091e5e55fedb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Konfirmasi kata sandi'])[1]/following::button[1]</value>
      <webElementGuid>5bbb6964-de4b-4963-b983-5cd5a7781842</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Daftar']/parent::*</value>
      <webElementGuid>285b226f-9949-4228-877f-9d6e9b751c89</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[8]/div/button</value>
      <webElementGuid>8c734cee-ee97-4a37-b69c-b96cd89d40c8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = 'buttonRegisterTrack' and @type = 'submit' and (text() = '
                                            Daftar
                                        ' or . = '
                                            Daftar
                                        ')]</value>
      <webElementGuid>a9505062-02a2-4153-9187-2f927eb2d246</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
